package nctuw.littledot.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseDatabase {
	protected Context mCtx;
	protected SQLiteOpenHelper DBHelper;
	protected SQLiteDatabase db;

	public BaseDatabase open() {
		if (db == null || !db.isOpen())
			db = DBHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		if (db.isOpen())
			db.close();
	}

	/*************/
	/** Queries **/
	/*************/

	public Cursor rawQuery(String sql) {
		return rawQuery(sql, null);
	}

	public Cursor rawQuery(String sql, String[] args) {
		Cursor cur = db.rawQuery(sql, args);
		return cur;
	}

	/*************/
	/** Inserts **/
	/*************/

	public long insert(String table, String[] keys, Object[] values) {
		long result = db.insert(table, null, toContentValues(keys, values));
		return result;
	}

	/*************/
	/** Updates **/
	/*************/

	public int update(String table, String[] keys, Object[] values,
			String whereClause) {

		return update(table, keys, values, whereClause, null);
	}

	public int update(String table, String[] keys, Object[] values,
			String whereClause, String[] whereArgs) {

		ContentValues kvPair = toContentValues(keys, values);
		int result = db.update(table, kvPair, whereClause, whereArgs);
		return result;
	}

	/**
	 * Single column version of
	 * {@link #update(String, String[], Object[], String)}.
	 * 
	 * @param table
	 * @param key
	 * @param value
	 * @param whereClause
	 * @return
	 */
	public int update(String table, String key, Object value,
			String whereClause) {
		return update(table, new String[] { key }, new Object[] { value },
				whereClause);
	}

	/*************/
	/** Deletes **/
	/*************/

	public int delete(String table, String whereClaus) {
		return delete(table, whereClaus, null);
	}

	public int delete(String table, String whereClaus, String[] whereArgs) {
		int result = db.delete(table, whereClaus, whereArgs);
		return result;
	}

	/*************/
	/** Utility **/
	/*************/

	public static ContentValues toContentValues(String[] keys, Object[] values) {
		if (keys.length != values.length)
			throw new IllegalArgumentException("The number of keys("
					+ keys.length + ") and values(" + values.length + ") are not equal.");

		ContentValues kv = new ContentValues();

		for (int i = 0; i < keys.length; i++) {
			if (values[i] instanceof String)
				kv.put(keys[i], (String) values[i]);
			else if (values[i] instanceof Integer)
				kv.put(keys[i], (Integer) values[i]);
			else if (values[i] instanceof Double)
				kv.put(keys[i], (Double) values[i]);
			else if (values[i] instanceof Float)
				kv.put(keys[i], (Float) values[i]);
			else if (values[i] instanceof Long)
				kv.put(keys[i], (Long) values[i]);
			else if (values[i] instanceof Short)
				kv.put(keys[i], (Short) values[i]);
			else if (values[i] instanceof Boolean)
				kv.put(keys[i], (Boolean) values[i]);
			else if (values[i] instanceof Byte)
				kv.put(keys[i], (Byte) values[i]);
			else if (values[i] instanceof byte[])
				kv.put(keys[i], (byte[]) values[i]);
			else
				throw new IllegalArgumentException("Incompatible ContentValues type "
						+ values[i].getClass().getName() + " at index " + i);
		}

		return kv;
	}

}
