package nctuw.littledot.util;

import android.database.Cursor;

public class Curser {
	public static byte[] getBlob(Cursor c, String key) {
		return c.getBlob(c.getColumnIndex(key));
	}

	public static double getDouble(Cursor c, String key) {
		return c.getDouble(c.getColumnIndex(key));
	}

	public static float getFloat(Cursor c, String key) {
		return c.getFloat(c.getColumnIndex(key));
	}

	public static int getInt(Cursor c, String key) {
		return c.getInt(c.getColumnIndex(key));
	}

	public static long getLong(Cursor c, String key) {
		return c.getLong(c.getColumnIndex(key));
	}

	public static short getShort(Cursor c, String key) {
		return c.getShort(c.getColumnIndex(key));
	}

	public static String getString(Cursor c, String key) {
		return c.getString(c.getColumnIndex(key));
	}
}
