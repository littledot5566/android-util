package nctuw.littledot.util;

import android.content.Context;
import android.widget.Toast;

public class Echo {
	public static void i(Context context, String msg) {
		Toast.makeText(context, Leg.e(msg), Toast.LENGTH_SHORT).show();
		Leg.i(msg);
	}

	public static void d(Context context, String msg) {
		Toast.makeText(context, Leg.e(msg), Toast.LENGTH_SHORT).show();
		Leg.d(msg);
	}

	public static void w(Context context, String msg) {
		Toast.makeText(context, Leg.e(msg), Toast.LENGTH_SHORT).show();
		Leg.w(msg);
	}

	public static void e(Context context, String msg) {
		Toast.makeText(context, Leg.e(msg), Toast.LENGTH_SHORT).show();

	}

	public static void a(Context context, String msg) {
		Toast.makeText(context, Leg.a(msg), Toast.LENGTH_SHORT).show();
	}
}
