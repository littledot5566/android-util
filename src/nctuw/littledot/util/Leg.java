package nctuw.littledot.util;

/**
 * 
 */

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

/**
 * A Log that format messages similar to LogCat
 * 
 * @author littledot
 */
public class Leg {
	private static final byte DEBUG_INFO = 1;
	private static final byte DEBUG_DBUG = 2;
	private static final byte DEBUG_WARN = 3;
	private static final byte DEBUG_EROR = 4;
	private static final byte DEBUG_ASRT = 5;

	public static String i(String msg) {
		return log(DEBUG_INFO, msg);
	}

	public static String d(String msg) {
		return log(DEBUG_DBUG, msg);
	}

	public static String w(String msg) {
		return log(DEBUG_WARN, msg);
	}

	public static String e(String msg) {
		return log(DEBUG_EROR, msg);
	}

	public static String a(String msg) {
		return log(DEBUG_ASRT, msg);
	}

	private static String log(byte level, String msg) {
		/*
		 * for (StackTraceElement e : Thread.currentThread().getStackTrace()) {
		 * Log.i("Leg", e.toString()); }
		 */

		StackTraceElement e = Thread.currentThread().getStackTrace()[4];

		String fullClassName = e.getClassName();

		String tag = new StringBuilder(fullClassName.substring(fullClassName
				.lastIndexOf(".") + 1)).append(":").append(e.getLineNumber())
				.toString();

		String str = new StringBuilder("[").append(e.getMethodName()).append("]: ")
				.append(msg).toString();

		switch (level) {
		case DEBUG_INFO:
			Log.i(tag, str);
			break;
		case DEBUG_DBUG:
			Log.d(tag, str);
			break;
		case DEBUG_WARN:
			Log.w(tag, str);
			break;
		case DEBUG_EROR:
			Log.e(tag, str);
			break;
		case DEBUG_ASRT:
			Log.println(Log.ASSERT, tag, str);
			break;
		}
		return str;
	}

	public static void dumpCursor(Cursor cur) {
		Leg.a(DatabaseUtils.dumpCursorToString(cur));
	}

	public static void dumpCurrentRow(Cursor cur) {
		Leg.a(DatabaseUtils.dumpCurrentRowToString(cur));
	}

}
